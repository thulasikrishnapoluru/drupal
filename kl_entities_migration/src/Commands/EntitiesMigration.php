<?php

namespace Drupal\kl_entities_migration\Commands;

use Drush\Commands\DrushCommands;
use Drupal\taxonomy\Entity\Term;
use Drupal\kl_account\Entity\KlAccount;
use Drupal\kl_subscription\Entity\KlSubscription;
use Drupal\kl_seat\Entity\KlSeat;
use Drupal\user\Entity\User;

/**
 * Drush command file.
 *
 * @package Drupal\kl_entities_migration\Commands
 */
class EntitiesMigration extends DrushCommands {

  /**
   * Drush command that migrates KL content.
   *
   * @param string $filename
   *   Argument with csv file name.
   * @param string $type
   *   Argument with csv entity type.
   * @command kl_entities_migration:migrate-kl-entities
   * @aliases mkle
   */
  public function migrateKlEntities($filename, $type = '') {
    // Validate the csv file.
    $status = $this->validateInputFile($filename);
    if ($status) {
      // Get structured array with csv content.
      $data = $this->parseCsv($filename, ',');
      // Migrate KL custom entities.
      try {
        // Migrate user entities.
        if ($data['users'] && ($type == 'users' || empty($type))) {
          foreach ($data['users'] as $kl_user) {
            if (!empty(trim($kl_user['mail']))) {
              $uid = $this->migrateUser($kl_user);
              $this->output()->writeln(dt('[@uid] User @mail migrated.',
              ['@uid' => $uid, '@mail' => $kl_user['mail']]));
            }
          }
        }

        // Migrate account entities.
        if ($data['accounts'] && ($type == 'accounts' || empty($type))) {
          foreach ($data['accounts'] as $kl_account) {
            if (!empty(trim($kl_account['email']))) {
              $aid = $this->migrateAccount($kl_account);
              $this->output()->writeln(dt('[@aid] Account @mail migrated.',
              ['@aid' => $aid, '@mail' => $kl_account['email']]));
            }
          }
        }
        // Migrate subscription entities.
        if ($data['subscriptions'] && ($type == 'subscriptions' || empty($type))) {
          foreach ($data['subscriptions'] as $kl_subscription) {
            if (!empty(trim($kl_subscription['email']))) {
              if (empty($kl_subscription['group_number'])) {
                $sid = $this->migrateSubscription($kl_subscription);
                $this->output()->writeln(dt('[@sid] Subscription @mail migrated.',
                ['@sid' => $sid, '@mail' => $kl_subscription['email']]));
              }
              else {
                if (strtoupper(trim($kl_subscription['admin_flag'])) == 'TRUE') {
                  $sid = $this->migrateSubscription($kl_subscription);
                  $this->output()->writeln(dt('[@sid] Subscription @mail migrated.',
                  ['@sid' => $sid, '@mail' => $kl_subscription['email']]));
                }
              }
            }
          }
        }
        // Migrate seat entities.
        if ($data['seats'] && ($type == 'seats' || empty($type))) {
          $group = $data['group'];
          foreach ($data['seats'] as $kl_seat) {
            if (!empty(trim($kl_seat['email']))) {
              if (empty($kl_seat['group_number'])) {
                $stid = $this->migrateSeat($kl_seat);
                $this->output()->writeln(dt('[@stid] Seat @mail migrated.',
                ['@stid' => $stid, '@mail' => $kl_seat['email']]));
              }
              else {
                $flag = strtoupper(trim($kl_seat['admin_flag']));
              if ($flag == 'FALSE' || empty($flag)) {
                $mail = array_column($group, $kl_seat['group_number']);
                // Add seat to subscription.
                $kl_account = $this->checkIfAccountExists($kl_seat['email']);
                $account = $this->checkIfAccountExists($mail[0]);
                $kl_subscription = $this->checkIfSubscriptionExists($account->id());
                if (is_numeric($kl_account->id()) && is_numeric($kl_subscription->id())) {
                  $seat = $this->checkIfSeatExists($kl_account->id());
                  if (!is_object($seat) && empty($seat)) {
                    $seat = KlSeat::Create();
                  }
                  $seat->set('curr_account', $kl_account->id());
                  $seat->set('subscription', $kl_subscription->id());
                  $seat->set('permission_type', $this->getKlPlus($kl_seat['kl_plus']));
                  $seat->set('status', $this->getSeatState($kl_seat['status']));
                  $seat->save();
                  $stid = (!empty($seat->id())) ? $seat->id() : FALSE;
                  $this->output()->writeln(dt('[@stid] Seat @mail migrated for subscription @sid.',
                  ['@stid' => $stid, '@mail' => $kl_seat['email'], '@sid' => $kl_subscription->id()]));
                }
                else {
                  $this->output()->writeln(dt('Account with email @mail not exists or Subscription for @smail not exists.',
                  ['@mail' => $kl_seat['email'], '@smail' => $mail[0]]));
                }
              }
              else {
                $stid = $this->migrateSeat($kl_seat);
                $this->output()->writeln(dt('[@stid] Seat @mail migrated.',
                ['@stid' => $stid, '@mail' => $kl_seat['email']]));
              }
              }
            }
          }
        }
      }
      catch (\Exception $e) {
        $this->output()->writeln(dt('KL entities migration failed'));
      }
    }
  }

  /**
   * Function to get KL Plus.
   *
   * @param string $value
   *   Account state.
   *
   * @return string
   *   State type.
   */
  public function getKlPlus($value) {
    $kl_plus = strtoupper(trim($value));
    if ($kl_plus == 'TRUE') {
      $flag = 'klplus';
    }
    elseif ($kl_plus == 'FALSE') {
      $flag = 'kl';
    }
    else {
      $flag = 'trial';
    }
    return $flag;
  }

  /**
   * Function to validate the file provided.
   */
  public function validateInputFile($filename) {
    if (!file_exists($filename) || !is_readable($filename)) {
      return $this->output()->writeln(dt("File doesn't exist or not readable."));
    }
    $info = pathinfo($filename);
    if ($info['extension'] != 'csv') {
      return $this->output()->writeln(dt('File must have .csv extension.'));
    }

    return TRUE;
  }

  /**
   * Function to parse csv content to structured array.
   *
   * @param string $name
   *   Name of csv file.
   * @param string $delimiter
   *   Delimiter for csv content.
   *
   * @return[]
   *   Structured csv data.
   */
  public function parseCsv($name, $delimiter) {
    $data = [];
    // Open the csv file for reading.
    if (($handle = fopen($name, 'r')) !== FALSE) {
      $header = NULL;
      while (($row = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        }
        else {
          $department_name = $this->cleanString($row[68]);
          $department = (!empty(trim($department_name))) ? $this->getTidByName(trim($department_name), 'department') : NULL;
          $department_term = (!empty($department)) ? ['target_id' => $department] : [];
          $industry = (!empty(trim($row[79]))) ? $this->getTidByName(trim($row[79]), 'industry') : NULL;
          $industry_term = (!empty($industry)) ? ['target_id' => $industry] : [];
          $role = (!empty(trim($row[69]))) ? $this->getTidByName(trim($row[69]), 'role') : NULL;
          $role_term = (!empty($role)) ? ['target_id' => $role] : [];
          $graduation_date = (!empty(trim($row[67]))) ? date('Y-m-d', strtotime($row[67])) : NULL;
          $marketing_consent = (strtolower(trim($row[57])) == 'true' || strtolower(trim($row[59])) == 'true') ? TRUE : FALSE;
          $agreement_consent = (!empty(trim($row[8]))) ? TRUE : FALSE;
          $newsletter_consent = ((trim($row[73]) == 'TRUE') || (trim($row[73]) == 'FALSE')) ? 1 : 0;
          $newsletter_onDate = ((!empty($newsletter_consent)) && (trim($row[73]) == 'TRUE')) ? time() : NULL;
          $newsletter_offDate = ((!empty($newsletter_consent)) && (trim($row[73]) == 'FALSE')) ? time() : NULL;
          $disabled_date = (!empty(trim($row[13]))) ? date('Y-m-d', strtotime($row[13])) : NULL;
          $users[] = [
            'name' => $row[72],
            'mail' => $row[72],
            'status' => 1,
            'field_first_name' => utf8_encode($row[2]),
            'field_last_name' => utf8_encode($row[3]),
            'field_job_title' => utf8_encode($row[70]),
            'field_company' => utf8_encode($row[78]),
            'field_major' => utf8_encode($row[66]),
            'field_univ_name' => utf8_encode($row[78]),
            'field_disabled_date' => $disabled_date,
            'field_agreement_consent' => [
              [
                'onoff' => $agreement_consent,
                'ondate' => strtotime($row[8]),
              ],
            ],
            'field_marketing_consent' => [
              [
                'onoff' => $marketing_consent,
                'ondate' => strtotime($row[58]),
                'offdate' => strtotime($row[60]),
              ],
            ],
            'field_newsletter_consent' => [
              [
                'onoff' => $newsletter_consent,
                'ondate' => $newsletter_onDate,
                'offdate' => $newsletter_offDate,
              ],
            ],
            'field_graduation_date' => $graduation_date,
            'field_address' => [
              [
                'country_code' => trim($row[80]),
                'address_line1' => utf8_encode($row[81]),
                'address_line2' => utf8_encode($row[82]),
                'locality' => utf8_encode($row[83]),
                'administrative_area' => utf8_encode($row[84]),
                'postal_code' => $row[86],
              ],
            ],
            'field_department' => [
              $department_term,
            ],
            'field_industry' => [
              $industry_term,
            ],
            'field_role' => [
              $role_term,
            ],
            'field_protiviti_contact' => utf8_encode($row[104]),
            'field_referral_source' => utf8_encode($row[102]),
            'field_referral_source_other' => utf8_encode($row[103]),
            'field_project_id' => utf8_encode($row[100]),
            'field_insala_id' => utf8_encode($row[106]),
            'field_course' => utf8_encode($row[65]),
            'field_comment_history' => utf8_encode($row[108]),
          ];

          $accounts[] = [
            'state' => $row[16],
            'email' => $row[72],
          ];

          $subscriptions[] = [
            'email' => $row[72],
            'name' => $row[72],
            'creation_method' => 'csv_import',
            'is_group' => $row[4],
            'state' => $row[16],
            'created_at' => $row[7],
            'expires_at' => $row[10],
            'account_type' => $row[4],
            'account_subtype' => $row[5],
            'admin_flag' => $row[17],
            'kl_plus' => $row[6],
            'group_number' => $row[34],
            'subscription_total' => utf8_encode($row[48]),
            'subscription_total_klplus' => utf8_encode($row[49]),
            'subscription_discount' => utf8_encode($row[50]),
            'subscription_tax' => utf8_encode($row[51]),
            'total_cost' => utf8_encode($row[52]),
            'payment_type' => utf8_encode($row[55]),
            'promotion_history' => utf8_encode($row[42]),
          ];

          if (strtoupper(trim($row[17])) == 'TRUE') {
            $group[] = [
              $row[34] => $row[72],
            ];
          }
          $seats[] = [
            'email' => $row[72],
            'kl_plus' => $row[6],
            'created_at' => $row[7],
            'status' => $row[16],
            'admin_flag' => $row[17],
            'group_number' => $row[34],
          ];
        }
      }
      // Close the csv file.
      fclose($handle);
    }
    $data = [
      'users' => $users,
      'accounts' => $accounts,
      'subscriptions' => $subscriptions,
      'seats' => $seats,
      'group' => $group,
    ];
    return $data;
  }

  /**
   * Create/Update user entity.
   *
   * @param string[] $data
   *   User related content.
   *
   * @return int
   *   User id.
   */
  public function migrateUser(array $data) {
    $kl_user = $this->checkIfUserExists($data['mail']);

    if (!is_object($kl_user) && empty($kl_user)) {
      $kl_user = User::create($data);
      $kl_user->enforceIsNew(TRUE);
    }
    foreach ($data as $key => $value) {
      $kl_user->set($key, $value);
    }
    $kl_user->save();

    return $kl_user->id();
  }

  /**
   * Check if user exists.
   *
   * @param string $mail
   *   The mail id of the user.
   *
   * @return Drupal\user\Entity\User
   *   KL User entity.
   */
  public function checkIfUserExists($mail) {
    $kl_user_obj = user_load_by_mail($mail);
    $kl_user = (!empty($kl_user_obj)) ? $kl_user_obj : FALSE;
    return $kl_user;
  }

  /**
   * Get taxonomy term id.
   *
   * @param string $name
   *   Term name.
   * @param string $vid
   *   Term vid.
   *
   * @return int
   *   Term id or 0 if none.
   */
  public function getTidByName($name, $vid) {
    $name = preg_replace('!\s+!', ' ', $name);
    $lev = $this->getLevenshtein($name, $vid);
    if (!empty($lev)) {
      $tid = $lev;
    }
    else {
      $term = Term::create([
        'name' => $name,
        'vid' => $vid,
      ]);
      $term->save();
      $this->output()->writeln(dt('Term @term created in vocabulary @vid.',
      ['@term' => $term->getName(), '@vid' => $vid]));
      $tid = $term->id();
    }

    return !empty($tid) ? $tid : NULL;
  }

  /**
   * Check matching term.
   *
   * @param string $name
   *   Term name.
   * @param string $vid
   *   Term vid.
   *
   * @return int
   *   Term id or 0 if none.
   */
  public function getLevenshtein($name, $vid) {
    $terms = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_names[$term->tid] = $term->name;
    }
    if (str_word_count($name) > 1) {
      $shortest = -1;
      foreach ($term_names as $word) {
        $lev = levenshtein($name, $word);
        if ($lev == 0) {
          $closest = $word;
          $shortest = 0;
          break;
        }

        if ($lev <= $shortest || $shortest < 0) {
          $closest = $word;
          $shortest = $lev;
        }
      }
      if ($shortest < 10) {
        $key = array_search($closest, $term_names);
      }
    }
    else {
      $key = array_search(preg_replace('/[^A-Za-z0-9\-]/', '', $name), $term_names);
    }

    return !empty($key) ? $key : NULL;
  }

  /**
   * Create/Update account entity.
   *
   * @param string[] $data
   *   Account related content.
   *
   * @return int
   *   KL Account Id.
   */
  public function migrateAccount(array $data) {
    $kl_user = $this->checkIfUserExists($data['email']);
    if (is_object($kl_user)) {
      $kl_account = $this->checkIfAccountExists($data['email']);
      if (!is_object($kl_account) && empty($kl_account)) {
        $kl_account = KlAccount::create();
      }
      $kl_account->set('user', $kl_user->id());
      $kl_account->set('email', $data['email']);
      $kl_account->set('state', $this->getState($data['state']));
      $kl_account->save();
      $aid = (!empty($kl_account->id())) ? $kl_account->id() : FALSE;
      return $aid;
    }
    else {
      $this->output()->writeln(dt('User with email @mail not exist.', ['@mail' => $data['email']]));
    }
  }

  /**
   * Function to get KL state type.
   *
   * @param string $value
   *   Account state.
   *
   * @return string
   *   State type.
   */
  public function getState($value) {
    $type = strtolower(trim($value));
    if ($type == 'active') {
      return 'active';
    }
    else {
      return 'canceled';
    }
  }

  /**
   * Function to get KL state type for seat.
   *
   * @param string $value
   *   Account state.
   *
   * @return string
   *   State type.
   */
  public function getSeatState($value) {
    $type = strtolower(trim($value));
    if ($type == 'active') {
      return 'taken';
    }
    else {
      return 'canceled';
    }
  }

  /**
   * Check if account exists.
   *
   * @param string $mail
   *   Email id of account.
   *
   * @return Drupal\kl_account\Entity\KlAccountInterface
   *   The Kl Account.
   */
  public function checkIfAccountExists($mail) {
    $account = \Drupal::entityTypeManager()
      ->getStorage('kl_account')
      ->loadByProperties(['email' => $mail]);
    $kl_account = (!empty($account)) ? reset($account) : FALSE;

    return $kl_account;
  }

  /**
   * Function to get KL subscription account type.
   *
   * @param string $type
   *   Account type.
   * @param string $subType
   *   Account subtype.
   * @param bool $adminFlag
   *   Admin flag.
   * @param bool $klPlus
   *   KL Plus.
   *
   * @return string
   *   The Kl Subscription Type .
   */
  public function getAccountType($type, $subType, $adminFlag, $klPlus) {
    $accountType = '';
    $type = strtoupper(trim($type));
    $subType = strtoupper(trim($subType));
    $flag = (strtoupper(trim($adminFlag)) == 'TRUE') ? 'TRUE' : 'FALSE';
    $klPlus = strtoupper(trim($klPlus));

    if (empty($klPlus) || $klPlus == 'FALSE' || $klPlus == '0') {
      $klPlus = 'FALSE';
    }
    switch (TRUE) {
      case ($type == 'TRIAL' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == 'TRIAL' && $subType = 'CLIENT TRIAL' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == 'TRIAL' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == 'GROUP TRIAL' && empty($subType) && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == 'GROUP TRIAL' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == '6-MONTH SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'alumni';
        break;

      case ($type == 'SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'TRUE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'SUBSCRIPTION' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'student';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && empty($subType) && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && empty($subType) && $flag == 'TRUE' && $klPlus == 'TRUE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'TRUE'):
        $accountType = 'subscriber';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'teacher';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'student';
        break;

      case ($type == 'SPECIAL' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'student';
        break;

      case ($type == 'SPECIAL' && $subType == 'EMPLOYEE' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'employee';
        break;

      case ($type == 'SPECIAL' && $subType == 'CLIENT TRIAL' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'client_trial';
        break;

      case ($type == 'SPECIAL' && $subType == 'COMPLIMENTARY' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'special_kl';
        break;

      case ($type == 'SPECIAL' && $subType == 'INTERN PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'intern';
        break;

      case ($type == 'SPECIAL' && $subType == 'EMPLOYEE' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'rh_employee';
        break;

      case ($type == 'SPECIAL' && $subType == 'RHI' && $flag == 'FALSE' && $klPlus == 'TRUE'):
        $accountType = 'rh_consultant';
        break;

      case ($type == 'SPECIAL' && $subType == 'COMPLIMENTARY' && $flag == 'FALSE' && $klPlus == 'TRUE'):
        $accountType = 'special_klp';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'PROTIVITI AFFILIATE' && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'member_firm';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'PROTIVITI AFFILIATE' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'member_firm';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'teacher';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'UNIVERSITY PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'student';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'INTERN PROGRAM' && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'intern';
        break;

      case ($type == 'GROUP SPECIAL' && $subType == 'INTERN PROGRAM' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'intern';
        break;

      case ($type == 'SPECIAL' && $subType == 'PROTIVITI AFFILIATE' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'member_firm';
        break;

      case ($type == 'SPECIAL' && $subType == 'RHI' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'rh_consultant';
        break;

      case ($type == 'GROUP SUBSCRIPTION' && $subType == 'PROTIVITI AFFILIATE' && $flag == 'FALSE' && $klPlus == 'FALSE'):
        $accountType = 'member_firm';
        break;

      case ($type == 'TRIAL' && $subType == 'TRIAL' && $flag == 'FALSE' && empty($klPlus)):
        $accountType = 'trial';
        break;

      case ($type == 'TRIAL' && empty($subType) && $flag == 'TRUE' && $klPlus == 'FALSE'):
        $accountType = 'trial';
        break;

      case ($type == '6-MONTH SUBSCRIPTION' && empty($subType) && $flag == 'FALSE' && $klPlus == 'TRUE'):
        $accountType = 'subscriber';
        break;

    }
    return $accountType;
  }

  /**
   * Function to get KL subscription group type.
   *
   * @param string $type
   *   Account type.
   *
   * @return bool
   *   TRUE if group, FALSE if not group .
   */
  public function isGroup($type) {
    $type = ucwords(trim($type));
    $groups = ["Group Trial", "Group Subscription", "Group Special"];
    if (in_array($type, $groups)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Create/Update subscription entity.
   *
   * @param string[] $data
   *   Subscription related content.
   *
   * @return int
   *   KL Subscription Id.
   */
  public function migrateSubscription(array $data) {
    $kl_account = $this->checkIfAccountExists($data['email']);
    if (is_object($kl_account)) {
      $kl_subscription = $this->checkIfSubscriptionExists($kl_account->id());
      if (!is_object($kl_subscription) && empty($kl_subscription)) {
        $kl_subscription = KlSubscription::create();
      }
      $created = strtotime($data['created_at']);
      $expired = strtotime($data['expires_at']);
      $account_type = $this->getAccountType($this->cleanString($data['account_type']), $data['account_subtype'], $data['admin_flag'], $data['kl_plus']);
      // Get Subscription Duration.
      if ($created && $expired) {
        $year1 = date('Y', $created);
        $year2 = date('Y', $expired);
        $month1 = date('m', $created);
        $month2 = date('m', $expired);
        $duration = (($year2 - $year1) * 12) + ($month2 - $month1);
      }
      $kl_subscription->set('name', substr($data['name'], 0, 50));
      $kl_subscription->set('type', $account_type);
      $kl_subscription->set('creation_method', 'csv_import');
      $kl_subscription->set('is_group', $this->isGroup($data['is_group']));
      $kl_subscription->set('state', $this->getState($data['state']));
      $kl_subscription->set('expires_at', date('Y-m-d\Th:i:s', $expired));
      $kl_subscription->set('duration', $duration);
      $kl_subscription->set('owner', $kl_account->id());
      $kl_subscription->set('subscription_total', $data['subscription_total']);
      $kl_subscription->set('subscription_total_klplus', $data['subscription_total_klplus']);
      $kl_subscription->set('subscription_discount', $data['subscription_discount']);
      $kl_subscription->set('subscription_tax', $data['subscription_tax']);
      $kl_subscription->set('total_cost', $data['total_cost']);
      $kl_subscription->set('payment_type', $data['payment_type']);
      $kl_subscription->set('promotion_history', $data['promotion_history']);
      $kl_subscription->save();
      $sid = (!empty($kl_subscription->id())) ? $kl_subscription->id() : FALSE;
      if (empty($account_type)) {
        $this->output()->writeln(dt('Subscription failed for @mail.', ['@mail' => $data['email']]));
      }
      return $sid;
    }
    else {
      $this->output()->writeln(dt('Account with email @mail not exists.', ['@mail' => $data['email']]));
    }
  }

  /**
   * Check if subscription exists.
   *
   * @param int $aid
   *   Account id.
   *
   * @return \Drupal\kl_subscription\Entity\KlSubscriptionInterface
   *   The referenced KL Subscription entity.
   */
  public function checkIfSubscriptionExists($aid) {
    $subscription = \Drupal::entityTypeManager()
      ->getStorage('kl_subscription')
      ->loadByProperties(['owner' => $aid]);
    $kl_subscription = (!empty($subscription)) ? reset($subscription) : FALSE;

    return $kl_subscription;
  }

  /**
   * Create/Update seat entity.
   *
   * @param string[] $data
   *   Seat related content.
   *
   * @return int
   *   KL Seat Id.
   */
  public function migrateSeat(array $data) {
    $kl_account = $this->checkIfAccountExists($data['email']);
    if (is_object($kl_account)) {
      $kl_subscription = $this->checkIfSubscriptionExists($kl_account->id());
      $kl_seat = $this->checkIfSeatExists($kl_account->id());
      if (!is_object($kl_seat) && empty($kl_seat)) {
        $kl_seat = KlSeat::Create();
      }
      $kl_seat->set('curr_account', $kl_account->id());
      $kl_seat->set('subscription', $kl_subscription->id());
      $kl_seat->set('permission_type', $this->getKlPlus($data['kl_plus']));
      $kl_seat->set('status', $this->getSeatState($data['status']));
      $kl_seat->save();
      $stid = (!empty($kl_seat->id())) ? $kl_seat->id() : FALSE;
      return $stid;
    }
    else {
      $this->output()->writeln(dt('Account with email @mail not exists.', ['@mail' => $data['email']]));
    }
  }

  /**
   * Check if Seat exists.
   *
   * @param int $aid
   *   Account id.
   *
   * @return bool
   *   The referenced KL Seat entity.
   */
  public function checkIfSeatExists($aid) {
    $seat = \Drupal::entityTypeManager()
      ->getStorage('kl_seat')
      ->loadByProperties(['curr_account' => $aid]);
    $kl_seat = (!empty($seat)) ? reset($seat) : FALSE;

    return $kl_seat;
  }

  /**
   * Check if Group Number exists.
   *
   * @param int $group_number
   *   Account id.
   *
   * @return int
   *   The Group Number if empty.
   */
  public function checkGroupNumber($group_number) {
    $group_number = (!empty($row[34])) ? reset($row[34]) : FALSE;

    return $group_number;
  }

  /**
   * Remove special characters.
   *
   * @param int $string
   *   String to be checked.
   *
   * @return string
   *   String without special characters.
   */
  public function cleanString($string) {
    $str = preg_replace("/[^A-Za-z0-9- ]/", '', $string);

    return $str;
  }

}
